﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Grawitacja : MonoBehaviour
{
	public GameObject prefab;
	public int iloscKulek = 0;
	public Text text;
	public int ileMaBycKulek = 10;
	public float forceMagnitude;
	public PointEffector2D point;
	public bool zmianaKierunku;
	public Kierunek kierunek;



	void Start() 
	{
		text = GetComponent<Text> ();

		InvokeRepeating("Spawn", 2.5f, 0.25f); 
		kierunek = GameObject.Find("Kierunek").GetComponent<Kierunek>();
	}

	public void Spawn()
	{
		if (iloscKulek < ileMaBycKulek)
		{
			Instantiate (prefab, new Vector3 (Random.Range(-18f,35f),Random.Range(-18f,30f), 0), Quaternion.identity);
			iloscKulek = 1 + iloscKulek;
			Licznik.scoreValue = iloscKulek;

		} 
		else 
		{
			// nie wiem jak to zatrzymać
			Debug.LogError ("nadal liczy");
		}
	}


	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Kulka")
		{
			
			Instantiate (prefab, new Vector3 (Random.Range(-18f,35f),Random.Range(-18f,30f), 0), Quaternion.identity);
			Destroy (this.gameObject);
			Debug.LogError ("stukniecie");
		}
	}
}