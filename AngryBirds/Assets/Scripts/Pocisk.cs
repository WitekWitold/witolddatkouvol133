﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocisk : MonoBehaviour {

	private bool mouse1;
	public Rigidbody2D rb2D;
	public Rigidbody2D rd2DSpawner;
	public Vector2 kierunek;
	Vector2 poczatkowa;
	public float promien = 2f;
	float sila;
	public float wzmocnienieSily = 2f;

	void Start ()
	{
		poczatkowa = rd2DSpawner.position;
	}

	void Update ()
	{

		if (mouse1)
		{
			
			Vector2 kursor = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			// maksymalny promien
			if (Vector3.Distance(kursor, poczatkowa) > promien)
				rb2D.position = poczatkowa + (kursor - poczatkowa).normalized * promien;
			else
				rb2D.position = kursor;
		}
	}


	void OnMouseDown ()
	{
		mouse1 = true;

	}

	void OnMouseUp ()
	{
		mouse1 = false;

		// kierunek i wartosc sily
		Vector2 kursor = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		sila = Vector3.Distance (kursor, poczatkowa);
		sila = sila * wzmocnienieSily;
		kierunek = poczatkowa - (Vector2)transform.position;
		//dodanie sily
		GetComponent<Rigidbody2D>().AddForce(kierunek * sila);
		// zmiana tagu dla wyszukiwania celu dla kamery
		transform.gameObject.tag = "StaryPocisk";

	}
}