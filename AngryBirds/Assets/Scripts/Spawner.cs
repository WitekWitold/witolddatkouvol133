﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public GameObject pociskPrefab;

	bool occupied = false;

	void FixedUpdate () 
	{		
		if (!occupied) 
		{				
			StartCoroutine(SpawnNext());
			occupied = true;
		}		
	}

	public IEnumerator SpawnNext() 
	{		// opoznienie 
		yield return new WaitForSeconds (1);
		// nowy pocisk
		Instantiate(pociskPrefab,new Vector3(-3.99f, 5.88f, 0f),Quaternion.identity);
		occupied = true;
	}

	void OnTriggerEnter2D(Collider2D co) 
	{
		occupied = false;
	}
}
